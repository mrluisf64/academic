<?php

use App\Alumno;
use App\Seccion;
use Faker\Generator as Faker;
use Illuminate\Database\Seeder;
use Maatwebsite\Excel\Facades\Excel;


class AlumnoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        Alumno::truncate();
        Excel::load('public/csv/list.csv', function ($reader) {
            foreach ($reader->get() as $alumno) {
                if ($alumno->nombre !== null && $alumno->nombre !== false) {
                    \App\Alumno::create([
                        'nombre' => $alumno->nombre,
                        'grado' => $alumno->grado,
                        'seccion' => $alumno->seccion,
                        'cedula' => $alumno->cedula,
                        'direccion' => $alumno->direccion,
                        'telefonos' => $alumno->telefonos
                    ]);
                }
            }
        });
    }
}
